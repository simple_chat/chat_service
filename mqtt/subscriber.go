package mqtt

import (
	"fmt"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

func (h *MqttHandle) MessageHandler() error {
	token := h.client.Subscribe(h.cfg.Topic, 0, h.readMessage)
	token.Wait()
	return token.Error()
}

func (h *MqttHandle) readMessage(client mqtt.Client, msg mqtt.Message) {
	topic := msg.Topic()
	payload := msg.Payload()
	fmt.Println("Recieved message: ", string(payload), "\n", "topic: ", topic)
}
