package mqtt

import (
	"fmt"
	"log"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"gitlab.com/chat_service/config"
)

type MqttHandle struct {
	client mqtt.Client
	cfg    *config.Config
}

func NewMqqtt(cfg config.Config) *MqttHandle {
	opts := mqtt.NewClientOptions()
	opts.AddBroker(fmt.Sprintf("tcp://%s:%s", cfg.Broker, cfg.Port))
	opts.SetClientID(cfg.ClientID)
	opts.SetUsername(cfg.UserName)
	opts.SetPassword(cfg.Password)
	client := mqtt.NewClient(opts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		log.Println("error connection mqtt: ", token.Error())
	}
	return &MqttHandle{
		client: client,
		cfg:    &cfg,
	}
}
