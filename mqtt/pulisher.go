package mqtt

func (h *MqttHandle) Publisher(data interface{}) error {
	token := h.client.Publish(h.cfg.Topic, 0, false, data)
	token.Wait()
	return token.Error()
}
