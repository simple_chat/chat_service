package main

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/chat_service/config"
	"gitlab.com/chat_service/mqtt"
)

func main() {
	cfg := config.Load()

	mqttHander := mqtt.NewMqqtt(cfg)

	err := mqttHander.MessageHandler()
	if err != nil {
		log.Println("Error messageHanler: ", err)
	}
	
	// err = mqttHander.Publisher("Salom, Men Mqtt ni o'rgandim!!!")
	// if err != nil {
	// 	log.Println("Error publisher: ", err)
	// }

	if err := http.ListenAndServe(cfg.HttpPort, nil); err != nil {
		fmt.Println("Error listen port: 5555", err)
	}
}
